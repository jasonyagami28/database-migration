<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateKomentarJawabanTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('komentar_jawaban', function (Blueprint $table) {
            $table->engine = 'InnoDB';
		
		    $table->bigIncrements('id');
		    $table->string('isi', 255)->nullable();
		    $table->date('tanggal_dibuat')->nullable();
		    $table->integer('jawaban_id')->unsigned();
		    $table->integer('profil_id')->unsigned();
		
		    $table->index('jawaban_id','fk_komentar_jawaban_jawaban1_idx');
		    $table->index('profil_id','fk_komentar_jawaban_profil1_idx');
		
		    $table->foreign('jawaban_id')
		        ->references('id')->on('jawaban');
		
		    $table->foreign('profil_id')
		        ->references('id')->on('profil');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('komentar_jawaban');
    }
}
