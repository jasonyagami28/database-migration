<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProfilTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('profil', function (Blueprint $table) {
            $table->engine = 'InnoDB';
		
		    $table->bigIncrements('id');
		    $table->string('nama_lengkap', 100)->nullable();
		    $table->string('email', 45)->nullable();
		    $table->string('foto', 45)->nullable();
		
		    $table->unique('id asc','id_unique');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('profil');
    }
}
