<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateKomentarPertanyaanTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('komentar_pertanyaan', function (Blueprint $table) {
            $table->engine = 'InnoDB';
		
		    $table->bigIncrements('id');
		    $table->string('isi', 255)->nullable();
		    $table->date('tanggal_dibuat')->nullable();
		    $table->integer('pertanyaan_id')->unsigned();
		    $table->integer('profil_id')->unsigned();
		
		    $table->index('pertanyaan_id','fk_komentar_pertanyaan_pertanyaan1_idx');
		    $table->index('profil_id','fk_komentar_pertanyaan_profil1_idx');
		
		    $table->foreign('pertanyaan_id')
		        ->references('id')->on('pertanyaan');
		
		    $table->foreign('profil_id')
		        ->references('id')->on('profil');
		
		    $table->timestamps();
        
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('komentar_pertanyaan');
    }
}
