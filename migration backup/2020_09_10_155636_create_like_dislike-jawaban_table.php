<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLikeDislikeJawabanTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('like_dislike_jawaban', function(Blueprint $table) {
		    $table->engine = 'InnoDB';
		
		    $table->integer('jawaban_id')->unsigned();
		    $table->integer('profil_id')->unsigned();
		    $table->integer('poin')->nullable();
		    
		    $table->primary('jawaban_id', 'profil_id');
		
		    $table->index('profil_id','fk_jawaban_has_profil_profil1_idx');
		    $table->index('jawaban_id','fk_jawaban_has_profil_jawaban1_idx');
		
		    $table->foreign('jawaban_id')
		        ->references('id')->on('jawaban');
		
		    $table->foreign('profil_id')
                ->references('id')->on('profil');
                
                $table->timestamps();
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('like_dislike_jawaban');
    }
}
